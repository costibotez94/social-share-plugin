# Social Share WordPress Plugin #

A plugin that will automatically display selected social network(s) sharing buttons in posts and/or on pages.

Support for the following social networks: 

* Facebook
* Twitter
* Google+
* Pinterest
* LinkedIn
* Whatsapp (for mobile browsers only).

The plugin options page includes the following configurable items:

* The choice to display on posts / pages / other registered custom post types
* Options to activate / deactivate the buttons for different social networks
* Three different button sizes to choose from (small / medium / large)
* The choice to display the icons in their original colors (default) or all in a selected color
* Options to place the social share bar (one or more of these can be selected)
* * below the post title
* * floating on the left area
* * after the post content
* * inside the featured image
* Support for i18n